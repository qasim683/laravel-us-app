FROM php:7.4-fpm




RUN apt-get update && apt-get install -y \
        libpng-dev \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        zip \
        curl \
        unzip \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install zip \
    && docker-php-source delete



RUN apt-get clean && rm -rf /var/lib/apt/lists/*


RUN docker-php-ext-install mbstring exif pcntl bcmath gd


COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer config -g repo.packagist composer https://packagist.org

#RUN useradd -G www-data,root -u $uid -d /home/$user $user
#RUN mkdir -p /home/$user/.composer && chown -R $user:$user /home/$user 
RUN chmod 777 -R /var/www


USER root

WORKDIR /var/www
COPY . .
RUN composer update
RUN chmod 777 -R ./*
RUN chmod 777 -R ./storage




